package jtm.extra01;

public class Zodiac {

	/**
	 * Determine the sign of the zodiac, use day and month.
	 * 
	 * @param day
	 * @param month
	 * @return zodiac
	 */
	private int day;
	private int month;
	
	
	
	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		if (day > 0 && day <= 31) {
		this.day = day;
		}
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		if (month <= 12 && month > 0) {
		this.month = month;
		}
	}

	public static String getZodiac(int day, int month) {
		String zodiac = null;
		// TODO #1: Implement method which return zodiac sign names
		// As method parameter - day and month;
		// Look at wikipedia:
		// https://en.wikipedia.org/wiki/Zodiac#Table_of_dates
		// Tropical zodiac, to get appropriate date ranges for signs
		switch (month) {
		case 1:
			return day > 20 ? "Aquarius" : "Capricorn";
		case 2:
			return day > 19 ? "Piscies" : "Aquarius";
		case 3:
			return day > 21 ? "Aries" : "Piscies";
		case 4:
			return day > 20 ? "Taurus" : "Aries";
		case 5:
			return day > 21 ? "Gemini" : "Taurus";
		case 6:
			return day > 21 ? "Cancer" : "Gemini";
		case 7:
			return day > 23 ? "Leo" : "Cancer";
		case 8:
			return day > 23 ? "Virgo" : "Leo"; 
		case 9:
			return day > 23 ? "Libra" : "Virgo";
		case 10:
			return day > 23 ? "Scorpio" : "Libra";
		case 11:
			return day > 23 ? "Sagittarius" : "Scorpio";
		case 12:
			return day > 22 ? "Capricorn" : "Sagittarius";
		}
		return zodiac;
	}

	public static void main(String[] args) {
		// HINT: you can use main method to test your getZodiac method with
		// different parameters
		System.out.println(getZodiac(1, 1));
	}

}
