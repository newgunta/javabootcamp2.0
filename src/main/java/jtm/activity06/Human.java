package jtm.activity06;

public class Human implements Humanoid {
    int stomach;
    boolean isAlive;
    int birthWeight;
    int weight;

    public Human() {
        birthWeight = BirthWeight;
        stomach = 0;
        isAlive = true;
        weight = birthWeight + stomach;
    }


    @Override
    public void eat(Integer food) {
        if (stomach == 0) {
            stomach += food;
            weight += stomach;
        }
    }

    @Override
    public Integer vomit() {
    	int tmp = stomach;
    	stomach = 0;
        return tmp;
    }

    @Override
    public String isAlive() {
        return isAlive ? "Alive" : "Dead";
    }

    @Override
    public String killHimself() {
        isAlive = false;
        return isAlive();
    }

    @Override
    public int getWeight() {
        return birthWeight + stomach;
    }

    @Override
    public String toString() {
        return "Human: " + getWeight() + " [" + stomach + "]";
    }


}
