package jtm.activity06;

public class Martian implements Alien, Cloneable, Humanoid {

	int weight;
	Object stomach;
	int birthWeight;
	
	public Martian() {
		weight = -1;
		birthWeight = -1;
	}
	
	@Override
	public void eat(Integer food) {
		eat((Object)food);
	}

	@Override
	public void eat(Object item) {
		if (stomach == null) {
			stomach = item;

			if (item instanceof Integer) {
				Integer tmp = (Integer) item;
				weight += tmp;
			}
			if (item instanceof Human) {
				Human tmp = (Human) item;
				tmp.isAlive = false;
				weight += tmp.getWeight();
			} 
			if (item instanceof Martian) {
				Martian tmp = (Martian) item;
				weight += tmp.getWeight();	
			}
		}
	}

	@Override
	public Object vomit() {
		Object tmp = stomach;
		weight = birthWeight;
		stomach = null;
		return tmp;
	}

	@Override
	public int getWeight() {
		// TODO Auto-generated method stub
		return weight;
	}

	@Override
	public String isAlive() {
		return "I AM IMMORTAL!";
	}

	@Override
	public String killHimself() {
		return "I AM IMMORTAL!";
	}
	
	@Override
	public String toString() {
		return "Martian: " + weight + " [" + stomach + "]";
	}
	
	@Override
    public Object clone() throws CloneNotSupportedException {
    return clone(this);
	}

	private Object clone(Object current) throws CloneNotSupportedException {
		return clone();
	}
	
	}
