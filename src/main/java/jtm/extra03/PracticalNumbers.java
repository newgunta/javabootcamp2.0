package jtm.extra03;

import java.util.*;

public class PracticalNumbers {

    List<Integer> divisors = new ArrayList<Integer>();
    // TODO Read article https://en.wikipedia.org/wiki/Practical_number
    // Implement method, which returns practical numbers in given range
    // including
    String practicalNumbers = "";
    public String getPracticalNumbers(int from, int to) {
        for (int i = from; i <= to; i++) {
            if (isPractical(i)) {
                practicalNumbers = practicalNumbers + i + ", ";
            }
        }
        if (practicalNumbers.length() > 2) {
            return practicalNumbers.substring(0, practicalNumbers.length() - 2);
        } else {
            return "";
        }
    }

    public boolean isPractical (int i) {
        if (i == 1 || i == 2) {
            return true;
        }
        if (i % 2 != 0) {
            return false;
        }
        for (int j = 1; j < i; j++) {
            if (i % j == 0) {
                divisors.add(j);
            }
        }
        for (int j = 2; j <= i; j+=2) {

            if (!divisors.contains(j) && !isSum(j)) {
                return false;
            }
         }
        return true;
    }

    private boolean isSum(int j) {
        int sum = 0;
        for (int i = 0; i < divisors.size(); i++) {
            for (int k = 0; k < divisors.size(); k++ ) {
                if (i != k) {
                    sum = divisors.get(i) + divisors.get(k);
                    if (sum == j / 2 || sum == j) {
                        return true;
                    }
                }
            }
        }
        for (int l = 0; l < divisors.size(); l++) {
            for (int i = 0; i < divisors.size(); i++) {
                for (int k = 0; k < divisors.size(); k++ ) {
                    if (i != k  && l != i && k != l) {
                        sum = divisors.get(i) + divisors.get(k) + divisors.get(l);
                        if (sum == j / 2 || sum == j) {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }
    }