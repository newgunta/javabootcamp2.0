package jtm.activity05;

import jtm.activity04.Road;
import jtm.activity04.Transport;

public class WaterRoad extends Road {

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "WaterRoad" + " " + super.toString();
	}

	public WaterRoad() {
		super();
		// TODO Auto-generated constructor stub
	}

	public WaterRoad(String from, String to, int distance) {
		super(from, to, distance);
		// TODO Auto-generated constructor stub
	}

}

class Ship extends Transport{
	protected byte numberOfSails;

	public Ship(String id, byte numberOfSails) {
		super(id);
		this.numberOfSails = numberOfSails;
		// TODO Auto-generated constructor stub
	}
	
	public String move(Road road) {
		if(road instanceof WaterRoad) {
		return super.toString2() + " is sailing on " + road.toString() 
		+ " with "+ numberOfSails + " sails";
		} else {
			return "Cannot sail on " + road.toString();
		}
		
	}
}

class Vehicle extends Transport {
	protected int wheels;

	public Vehicle(String id, float consumption, int tankSize, int wheels) {
		super(id, consumption, tankSize);
		this.wheels = wheels;
		// TODO Auto-generated constructor stub
	}
	
	public String move(Road road) {
		if(road.getClass() == Road.class) {
		return super.toString2() + " is driving on " 
		+ road.toString() + " with " + wheels + " wheels";
		} else {
		return "Cannot drive on " + super.toString();
		}
	}	
}

class Amphibia extends Transport {
	private Ship ship;
	private Vehicle vehicle;
	private byte numberOfSails;
	private int wheels;
	

	public Amphibia(String id, float consumption, int tankSize, byte sails, int wheels) {
		super(id, consumption, tankSize);
		this.numberOfSails = sails;
		this.wheels = wheels;
		// TODO Auto-generated constructor stub
	}
	
	public String move(Road road) {
		if(road.getClass() == Road.class) {
		return ship.move(road);
		} else if(road instanceof WaterRoad) {
		return vehicle.move(road);
	}
		return null;
}
}